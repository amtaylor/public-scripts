<#

#>
#Lists Appnames here. 
#These AppxPackages are pulled from Windows 10 1809. They will need to be updated for other builds of Windows 10
function Service-WIM {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True)]
        [string[]]
        $Wim,

        [Parameter(Mandatory = $True)]
        [int[]]
        $Index = 3,

        [Parameter(Mandatory = $True)]
        [String[]]
        $MountPoint
    )
    
    begin {
        $appname = @(
            "Microsoft.YourPhone_2018.727.2137.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.MicrosoftOfficeHub_2017.1219.520.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.MicrosoftSolitaireCollection_4.1.5252.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.MixedReality.Portal_2000.18081.1242.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.Office.OneNote_16001.10228.20003.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.OneConnect_5.1807.1991.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.People_2018.516.2011.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.SkypeApp_14.26.95.0_neutral_~_kzf8qxf38zg5c"
            "Microsoft.Wallet_2.2.18179.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.GetHelp_10.1706.10441.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.Getstarted_6.13.11581.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.Microsoft3DViewer_4.1808.15012.0_neutral_~_8wekyb3d8bbwe"
            "microsoft.windowscommunicationsapps_2015.9330.21365.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.WindowsFeedbackHub_2018.822.2.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.XboxApp_41.41.18001.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.XboxGameOverlay_1.32.17005.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.XboxGamingOverlay_2.20.22001.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.XboxIdentityProvider_12.44.20001.0_neutral_~_8wekyb3d8bbwe"
            "Microsoft.XboxSpeechToTextOverlay_1.17.29001.0_neutral_~_8wekyb3d8bbwe"
        )
        
    }
    
    process {
        Write-Verbose "Checking for DISM cmdlets"
        If ((Get-Module -Name dism) -eq $false) {
            Try {
                Write-Verbose "Attemting to install dism module"
                Install-Module -Name dism
            }
            Catch {
                Write-error "DISM module not installed! Please load the DISM module to run this script"
                exit
            }
        }
        Write-Verbose "Testing if WIM path is valid"
        If ((Test-Path -Path "$WIM") -eq $False) {
            Write-Error "Path to WIM not found! Please check to see if the path is valid, then try again"
            exit
        }
        Write-Verbose "Check to see if mount path exists"
        If ((Test-Path -Path "$MountPoint") -eq $False) {
            Try {
                Write-Verbose "Path not found! Attempting to Create"
                New-Item -ItemType Directory -Path "$MountPoint"
            }
            Catch {
                Write-Error "Unable to create $MountPoint!"
                Exit
            }
        }
        Write-Verbose "Mounting Image"
        Mount-WindowsImage -ImagePath "$WIM" -Index "$Index" -Path "$MountPoint"
        Write-Verbose "Removing Appx Packages"
        ForEach ($app in $appname) {
            Write-Verbose "Removing $app"
            Remove-AppxProvisionedPackage -PackageName $App -Path "$MountPoint" -ErrorAction SilentlyContinue
        }
    }
    
    end {
        Write-Verbose "Saving Changes"
        Dismount-WindowsImage -Path "$MountPoint" -Save
    }
}